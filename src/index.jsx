import React from 'react'
import { render } from 'react-dom'

import { Welcome } from 'components'

render(<Welcome />, document.getElementById('root'))

import React from 'react'

import './style.css'
// import * as S from './styled'

const Welcome = () => {
  return (
    // The double div is only because without it
    // the parentheses would be omitted by Prettier
    // which we don't want, because we want to
    // demonstrate how function returns work
    <div>
      <div className="welcome">Welcome to React.js</div>
      {/* <S.Wrapper>Welcome to React.js</S.Wrapper> */}
    </div>
  )
}

export default Welcome
